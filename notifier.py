#!/usr/bin/env python

__version__ = 'notifier v00003.2'

import sys, os, time, re, signal, atexit, logging, subprocess, threading, smtplib, socket
try: from email.mime.text import MIMEText
except ImportError:
    from email.MIMEText import MIMEText

def usage():
  print sys.argv[0] + " file"
  """
  redhat linux - /var/log/messages

  Best practice for running this program:
  ./notifier.py /var/log/messages >>notifier.log 2>&1 &

  """

level = logging.INFO
#level = logging.DEBUG

MAIL_TO = ['karl@usaepay.com','servers@usaepay.com']
THIS_SERVER = socket.gethostname()
MAIL_FROM = THIS_SERVER + '@usaepay.com'
SMTP_SERVER = 'localhost'
SMTP_PORT = 25
SMTP_SSL = False
SMTP_AUTH = False
SMTP_USERNAME = ''
SMTP_PASSWORD = ''



hostname = os.uname()[1]
format = "%(asctime)s " + hostname + " %(filename)s %(levelname)s: %(message)s"
datefmt = "%b %d %H:%M:%S"
logging.basicConfig(level=level, format=format, datefmt=datefmt)
console = logging.StreamHandler(sys.stdout)
logging.getLogger(sys.argv[0]).addHandler(console)

def ticker():
  while (sigterm == False):
    logging.debug("sigterm: " + str(sigterm))
    time.sleep(1)

def cleanup():
    logging.info("cleanup:")

def follow(cmd):
  try:
    process = subprocess.Popen(cmd, shell=False, stdin=subprocess.PIPE,
                                                 stdout=subprocess.PIPE,
                                                 stderr=subprocess.STDOUT)
    logging.debug("process.pid: %s", process.pid)
    while (process.returncode == None):
      line = process.stdout.readline()
      if not line or sigterm == True:
        break
      else:
        yield line
        sys.stdout.flush()
  except:
    #exec_info = sys.exc_info()
    logging.debug("process exception")
    try:
      logging.debug(sys.version_info)
      if sys.version_info < (2, 6):
        os.kill(process.pid, signal.SIGKILL)
      else:
        process.terminate()
      logging.debug("process terminate")
    except:
      import traceback
      print >> sys.stderr, "Error in process: "
      traceback.print_exc()
    raise sys.exc_info[0], sys.exc_info[1], sys.exc_info[2]
  if process.poll() == None:
    os.kill(process.pid, signal.SIGKILL)
    logging.debug("process SIGKILL")

def send_notification(from_email, to_email, subject, message, smtp_server,
                      smtp_port, use_ssl, use_auth, smtp_user, smtp_pass):
  msg = MIMEText(message)

  msg['From'] = from_email
  msg['To'] = ', '.join(to_email)
  msg['Subject'] =  subject

  if(use_ssl):
      mailer = smtplib.SMTP_SSL(smtp_server, smtp_port)
  else:
      mailer = smtplib.SMTP(smtp_server, smtp_port)

  if(use_auth):
      mailer.login(smtp_user, smtp_pass)

  mailer.sendmail(from_email, to_email, msg.as_string())
  mailer.close()

def send_email(message=''):
  try:
    send_notification(MAIL_FROM, MAIL_TO, 'Notifier Notification: ' + THIS_SERVER,
                        str(message), SMTP_SERVER, SMTP_PORT, SMTP_SSL, SMTP_AUTH,
                        SMTP_USERNAME, SMTP_PASSWORD)
  except Exception, e:
    errorMessage = "Unable to send notification: %s" % e
    logging.info(errorMessage)




if __name__ == "__main__":
  try:
    filename = sys.argv[1]
  except IndexError:
    usage()

  sigterm = False
  atexit.register(cleanup) #python can't catch SIGKILL (kill -9)
  signal.signal(signal.SIGTERM, lambda signum, stack_frame: sys.exit(1))

  logging.info("Startup")

  #some log values to match/search for
  re_audit = re.compile(r'Audit',re.I)
  re_login = re.compile(r'Login',re.I)
  re_severity = re.compile(r'Severity',re.I)
  re_messageid = re.compile(r'MessageID',re.I)
  re_webcgi = re.compile(r'webcgi',re.I)
  re_failure = re.compile(r'failure',re.I)
  re_LOGIN_FAILURE = re.compile(r'LOGIN_FAILURE',re.I)
  re_cmc = re.compile(r'CMC-',re.I)

  #re_invalid_user = re.compile(r'Invalid user',re.I)
  #re_failed_password = re.compile(r'Failed password',re.I)

  watcher = threading.Thread(target = ticker, name = "ticker")
  watcher.setDaemon(1)
  watcher.start()

  while (sigterm == False):
    try:
      for line in follow(['tail', '-F',  filename]):

        #print str(type(line))
        #print str(line)
        #print len(line)

        #line = line.split()
        #print str(line)

        #Oct 16 11:22:28 bc1-ca4-01 Severity: Informational, Category: Audit, MessageID: USR0030, Message: Successfully logged in using krink, from 10.31.234.20 and GUI.
        try:
          if re_severity.search(line) and re_audit.search(line) and re_messageid.search(line):
            logging.info(line)
            send_email(line)
        except TypeError, e:
          errorMessage = "TypeError: %s" % e
          print str(errorMessage + str(line))

        #Oct 16 13:54:22 CMC-CA5-01 webcgi: Login success via LDAP, from 10.31.234.20 (username=krink, type=GUI)
        try:
          if re_webcgi.search(line):
            logging.info(line)
            send_email(line)
        except TypeError, e:
          errorMessage = "TypeError: %s" % e
          print str(errorMessage + str(line))

        #Oct 17 19:02:18 switch-ma1-04 switch-ma1-04: %STKUNIT0-M:CP %SEC-3-LOGIN_FAILURE: Login failure on line vty0 ( 10.31.191.18 )
        try:
          if re_LOGIN_FAILURE.search(line) and re_login.search(line) and re_failure.search(line):
            logging.info(line)
            send_email(line)
        except TypeError, e:
          errorMessage = "TypeError: %s" % e
          print str(errorMessage + str(line))

        #Oct 17 15:13:59 CMC-CA5-02 sshd[29446]: Login success via LDAP, from 192.168.247.120 (username=krink, type=SSH)
        try:
          if re_cmc.search(line) and re_login.search(line):
            logging.info(line)
            send_email(line)
        except TypeError, e:
          errorMessage = "TypeError: %s" % e
          print str(errorMessage + str(line))


    except (KeyboardInterrupt, SystemExit, Exception):
      sigterm = True
      watcher.join()
      cleanup()
      logging.info("Shutdown: " + str(sigterm))
      sys.exit(1)

## Notes ##

#Exception TypeError: "'builtin_function_or_method' object has no attribute '__getitem__'" in <generator object follow at 0x7f1885c75910> ignored
#print len(line), either line.split() string first, and/or just don't do exact match = line.split()[12]

#python 2.7.5
#Exception NameError: "global name 'exc_info' is not defined" in <generator object follow at 0x7fd15049a910> ignored

#python 2.4 SyntaxError: 'yield' not allowed in a 'try' block with a 'finally' clause
#Duplicate finally block
# try:
#  yield 42
# finally:
#  do_something()
#Becomes...
# try:
#  yield 42
# except:  # bare except, catches *anything*
#  do_something()
#  raise  # re-raise same exception
# do_something()
#
####
#python 2.4 AttributeError: 'Popen' object has no attribute 'terminate'

